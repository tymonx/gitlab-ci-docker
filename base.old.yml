# Copyright 2022 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---
.docker-script: &docker-script |-
    #!/usr/bin/env sh
    #
    # Copyright 2022 Tymoteusz Blazejczyk
    #
    # Licensed under the Apache License, Version 2.0 (the "License");
    # you may not use this file except in compliance with the License.
    # You may obtain a copy of the License at
    #
    #     http://www.apache.org/licenses/LICENSE-2.0
    #
    # Unless required by applicable law or agreed to in writing, software
    # distributed under the License is distributed on an "AS IS" BASIS,
    # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    # See the License for the specific language governing permissions and
    # limitations under the License.

    # Check for command errors
    set -e

    # It returns project directory path
    get_project_dir() {
        if [ -n "${CI_PROJECT_DIR}" ]; then
            echo "${CI_PROJECT_DIR}"
            return
        fi

        if path="$(git rev-parse --show-toplevel 2>/dev/null)" && [ -d "${path}" ]; then
            echo "${path}"
            return
        fi

        pwd
    }

    # It lowers all characters
    lower() {
        echo "$1" | tr '[:upper:]' '[:lower:]'
    }

    # It strips variable from leading and trailing whitespaces
    strip() {
        sed -r -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
    }

    # It checks if contains true
    is_true() {
        test "$(echo "$1" | strip | grep -ciE '^(on|yes|true|enable|[1-9][0-9]*)$')" -ne 0
    }

    # It colors to green
    green() {
        printf '\e[1;32m%s\e[0m\n' "$*"
    }

    # It colors to red
    red() {
        printf '\e[1;31m%s\e[0m\n' "$*"
    }

    # It colors to cyan
    cyan() {
        printf '\e[1;36m%s\e[0m\n' "$*"
    }

    # It colors to yellow
    yellow() {
        printf '\e[1;33m%s\e[0m\n' "$*"
    }

    # It prints info text (cyan color)
    info() {
        cyan "$@"
    }

    # It prints warning text (yellow color)
    warning() {
        yellow "$@"
    }

    # It prints error text (red color)
    error() {
        red "$@"
    }

    # It prints error text and exit (red color)
    fatal() {
        red "$@"
        exit 1
    }

    # It prints pass text (green color)
    pass() {
        green "$@"
    }

    # It prints fail text (red color)
    fail() {
        if is_true "${DOCKER_ALLOWED_TO_FAIL:-}"; then
            red "$@" " and allowed to fail. Shame on you!"
        else
            red "$@"
            DOCKER_FAILED=1
        fi
    }

    # It returns item list
    item() {
        printf '%s ' "$*"
    }

    # It returns true if file is compose
    is_yaml() {
        if [ ! -f "$1" ]; then
            return 1
        fi

        case "$(lower "$1")" in
            *.yml|*.yaml)
                return 0;;
            *)
                return 1;;
        esac
    }

    # It returns true if value is boolean
    is_boolean() {
        case "$(lower "$1")" in
            false|true)
                return 0;;
            *)
                return 1;;
        esac
    }

    # It returns true if value is number
    is_number() {
        test "$(echo "$1" | grep -cE '^[0-9]+$')" -ne 0
    }

    # It returns true if empty
    is_empty() {
        [ "$1" = "" ]
    }

    # It tranforms to lower camel case
    to_lower_camel_case() {
        tr '_' '\n' | \
        tr '[:upper:]' '[:lower:]' | \
        awk '{print toupper(substr($0, 1, 1)) substr($0, 2)}' | \
        tr -d '\n' | \
        awk '{print tolower(substr($0, 1, 1)) substr($0, 2)}'
    }

    # It returns true if the .dockerignore file is required
    is_dockerignore_required() {
        test "$(grep -iE '^[[:space:]]*copy[[:space:]]+[[:graph:]]+.*$' "$1" | grep -cv -- '--from')" -ne 0
    }

    # It returns Dockerfiles
    get_dockerfiles() {
        if [ "$#" -eq 0 ]; then
            get_dockerfiles "$(get_project_dir)"
            return
        fi

        for item in "$@"; do
            if [ -f "${item}" ] && ! is_yaml "${item}"; then
                echo "${item}"
            elif [ -d "${item}" ]; then
                find "${item}" -type f -iname dockerfile
            fi
        done
    }

    # It returns composes
    get_composes() {
        if [ "$#" -eq 0 ]; then
            get_composes "$(get_project_dir)"
            return
        fi

        for item in "$@"; do
            if is_yaml "${item}"; then
                echo "${item}"
            elif [ -d "${item}" ]; then
                find "${item}" \
                    -type f \
                    -iname compose.yml -o \
                    -iname compose.yaml -o \
                    -iname docker-compose.yml -o \
                    -iname docker-compose.yaml
            fi
        done
    }

    # It returns composes override
    get_composes_override() {
        find "$(dirname "$1")" \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -iname 'compose.*.yml' -o \
            -iname 'compose.*.yaml' -o \
            -iname 'docker-compose.*.yml' -o \
            -iname 'docker-compose.*.yaml' | sort
    }

    # It returns the latest git tag
    get_version() {
        git describe --exact-match --abbrev=0 2>/dev/null || echo "${CI_COMMIT_TAG:-v0.0.0}" | sed 's/^v//'
    }

    # It slugs string
    slug() {
        sed 's/[^a-zA-Z0-9._-]/-/g' | sed 's/-\+/-/g' | sed 's/^[\.-]\+//'
    }

    # It returns job name
    get_job_name() {
        echo "${CI_JOB_NAME:-}"
    }

    # It returns branch name
    get_branch_name() {
        echo "${CI_COMMIT_BRANCH:-${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-${CI_COMMIT_REF_NAME:-}}}"
    }

    # It returns cache tags
    get_cache_tag() {
        echo "dev-$(get_branch_name)-$(get_job_name)-test" | slug
    }

    # It returns cache tags
    get_cache_tags() {
        echo "${DOCKER_PULL_TAG:-}" "$(get_cache_tag)" edge latest "$(get_version)"
    }

    # It returns pull tag
    get_pull_tag() {
        echo "${DOCKER_PULL_TAG:-dev-${DOCKER_TAG:-latest}-${CI_PIPELINE_ID:-0}-test}"
    }

    # It returns pull image
    get_pull_image() {
        echo "${DOCKER_PULL_IMAGE:-${DOCKER_IMAGE:-${CI_REGISTRY_IMAGE:-}}}"
    }

    # It returns pull password
    get_pull_password() {
        echo "${DOCKER_PULL_PASSWORD:-${DOCKER_PASSWORD:-${CI_REGISTRY_PASSWORD:-}}}"
    }

    # It returns pull username
    get_pull_username() {
        echo "${DOCKER_PULL_USERNAME:-${DOCKER_USERNAME:-${CI_REGISTRY_USER:-}}}"
    }

    # It returns pull registry
    get_pull_registry() {
        echo "${DOCKER_PULL_REGISTRY:-${DOCKER_REGISTRY:-${CI_REGISTRY:-}}}"
    }

    # It returns push image
    get_push_image() {
        echo "${DOCKER_PUSH_IMAGE:-${DOCKER_IMAGE:-${CI_REGISTRY_IMAGE:-}}}"
    }

    # It returns push password
    get_push_password() {
        echo "${DOCKER_PUSH_PASSWORD:-${DOCKER_PASSWORD:-${CI_REGISTRY_PASSWORD:-}}}"
    }

    # It returns push username
    get_push_username() {
        echo "${DOCKER_PUSH_USERNAME:-${DOCKER_USERNAME:-${CI_REGISTRY_USER:-}}}"
    }

    # It returns push registry
    get_push_registry() {
        echo "${DOCKER_PUSH_REGISTRY:-${DOCKER_REGISTRY:-${CI_REGISTRY:-}}}"
    }

    # It returns push tags
    get_push_tags() {
        echo "${DOCKER_PUSH_TAG:-dev-${DOCKER_TAG:-latest}-${CI_PIPELINE_ID:-0}-test}"
    }

    # It returns push tag
    get_push_tag() {
        get_push_tags | awk '{print $1}'
    }

    # It logins to image registry
    docker_login() {
        if [ -n "$3" ]; then
            echo "$3" | docker login ${1:+--username "$1"} --password-stdin ${2:+"$2"}
        else
            docker login ${1:+--username "$1"} ${2:+"$2"}
        fi
    }

    # It pulls image from registry
    docker_pull() {
        docker_login "$(get_pull_username)" "$(get_pull_registry)" "$(get_pull_password)"
        docker pull "$(get_pull_image):${1:-dev-${DOCKER_TAG:-latest}-${CI_PIPELINE_ID:-0}-test}"
    }

    # It pushes image to registry
    docker_push() {
        if [ "$1" = "-" ]; then
            set -- "$(get_version)"
        fi

        if [ -z "$1" ]; then
            return
        fi

        docker tag "$(get_pull_image):$(get_pull_tag)" "$(get_push_image):$1"
        docker_login "$(get_push_username)" "$(get_push_registry)" "$(get_push_password)"
        docker push "$(get_push_image):$1"
    }

    # It returns cache image
    get_cache_image() {
        for tag in $(get_cache_tags); do
            if docker_pull "${tag}" >/dev/null 2>&1; then
                echo "$(get_pull_image):${tag}"
                return
            fi
        done
    }

    # It returns true if docker uses TLS
    docker_is_tls() {
        [ -n "${DOCKER_TLS_CERTDIR:-}" ] && \
        [ -s "${DOCKER_TLS_CERTDIR:-}/client/ca.pem" ] && \
        [ -s "${DOCKER_TLS_CERTDIR:-}/client/cert.pem" ] && \
        [ -s "${DOCKER_TLS_CERTDIR:-}/client/key.pem" ]
    }

    # It setups host
    docker_setup_host() {
        if [ -n "${DOCKER_HOST:-}" ]; then
            return
        fi

        if [ -S /var/run/docker.sock ]; then
            export DOCKER_HOST=unix:///var/run/docker.sock
        elif [ -S "${XDG_RUNTIME_DIR:-/run/user/$(id -u)}/docker.sock" ]; then
            export DOCKER_HOST="unix://${XDG_RUNTIME_DIR:-/run/user/$(id -u)}/docker.sock"
        elif docker_is_tls || [ -n "${DOCKER_TLS_VERIFY:-}" ]; then
            export DOCKER_HOST="tcp://docker:2376"
        else
            export DOCKER_HOST="tcp://docker:2375"
        fi
    }

    # It setups TLS
    docker_setup_tls() {
        case "${DOCKER_HOST:-}" in
            tcp:*)
                ;;
            *)
                return;;
        esac

        if [ -z "${DOCKER_TLS_VERIFY:-}" ] && [ -z "${DOCKER_CERT_PATH:-}" ] && docker_is_tls; then
            export DOCKER_TLS_VERIFY=1
            export DOCKER_CERT_PATH="${DOCKER_TLS_CERTDIR:-}/client"
        fi
    }

    # It setups docker
    docker_setup() {
        docker_setup_host
        docker_setup_tls
    }

    # It runs docker-compose config
    docker_lint_run_compose() {
        options=""

        for compose in $(get_composes_override "$1"); do
            options="${options} --file ${compose}"
        done

        docker-compose --file "$1" ${options:+${options:-}} ${DOCKER_COMPOSE_ARGUMENTS:+${DOCKER_COMPOSE_ARGUMENTS:-}} config --quiet
    }

    # It runs dockerfilelint
    docker_lint_run_dockerfilelint() {
        dockerfilelint ${DOCKERFILELINT_ARGUMENTS:+${DOCKERFILELINT_ARGUMENTS:-}} "$1"
    }

    # It runs dockerfilelinter
    docker_lint_run_dockerfilelinter() {
        DOCKERLINTER_ARGUMENTS="${DOCKERLINTER_ARGUMENTS:---ignore ER0012,ER0015}"
        dockerfilelinter --error ${DOCKERLINTER_ARGUMENTS:+${DOCKERLINTER_ARGUMENTS:-}} --file "$1"
    }

    # It runs hadolint
    docker_lint_run_hadolint() {
        hadolint --failure-threshold ignore "$1"
    }

    # It runs dockerignore check
    docker_lint_run_dockerignore() {
        file="$(dirname "$1")/.dockerignore"

        if [ ! -f "${file}" ]; then
            if ! is_dockerignore_required "$1"; then
                return 0
            fi

            echo "The .dockerignore file is missing when using ADD or COPY statements in the Dockerfile"
            return 1
        fi

        if [ ! -s "${file}" ]; then
            echo "The .dockerignore file is empty." \
                "At least add a special wildcard string ** at the top of the file to ignore everything on default"
            return 1
        fi

        if [ "$(grep -cE '^\*\*' "${file}")" -eq 0 ]; then
            echo "The .dockerignore file should have a special wildcard string ** at the top of the file to ignore everything on default"
            return 1
        fi

        if [ "$(sed 's/[[:space:]]*#.*$//' "${file}" | sed '/^[[:space:]]*$/d' | sed '/^\*\*/q' | wc -l)" -ne 1 ]; then
            echo "The .dockerignore file should have a special wildcard string ** at the top of the file before everything else"
            return 1
        fi

        return 0
    }

    # It lints Dockerfiles
    docker_lint_dockerfiles() {
        for dockerfile in $(get_dockerfiles "$@"); do
            if ! output="$(docker_lint_run_dockerignore "${dockerfile}" 2>&1)"; then
                red ".dockerignore issues for ${dockerfile}"
                echo "${output}"
                status=1
            fi

            if ! output="$(docker_lint_run_dockerfilelint "${dockerfile}" 2>&1)"; then
                red "dockerfilelint detected issues for ${dockerfile}"
                echo "${output}"
                status=1
            fi

            if ! output="$(docker_lint_run_dockerfilelinter "${dockerfile}" 2>&1)"; then
                red "dockerfilelinter detected issues for ${dockerfile}"
                echo "${output}"
                status=1
            fi

            if ! output="$(docker_lint_run_hadolint "${dockerfile}" 2>&1)"; then
                red "hadolint detected issues for ${dockerfile}"
                echo "${output}"
                status=1
            fi
        done

        return "${status:-0}"
    }

    # It lints composes
    docker_lint_composes() {
        for compose in $(get_composes "$@"); do
            if ! output="$(docker_lint_run_compose "${compose}" 2>&1)"; then
                red "docker-compose config detected issues for ${compose}"
                echo "${output}"
                status=1
            fi
        done

        return "${status:-0}"
    }

    # It lints Dockerfiles and compose files
    docker_lint_run() {
        status=0

        if ! docker_lint_dockerfiles "$@"; then
            status=1
        fi

        if ! docker_lint_composes "$@"; then
            status=1
        fi

        return "${status:-0}"
    }

    # It lints dockerfile
    docker_lint() {
        info "Running Docker lint..."

        if docker_lint_run "$@"; then
            pass "Lint passed"
        else
            fail "Lint failed"
        fi
    }

    # It builds image
    docker_build() {
        if [ -z "${CI:-}" ]; then
            exec docker build "${@:-.}"
        fi

        cache="$(get_cache_image)"

        docker_login "$(get_pull_username)" "$(get_pull_registry)" "$(get_pull_password)"

        docker build \
            ${cache:+--cache-from "${cache}"} \
            --label maintainer="${DOCKER_LABEL_MAINTAINER:-${GITLAB_USER_EMAIL:-}}" \
            --label org.label-schema.url="${DOCKER_LABEL_URL:-${CI_PROJECT_URL:-}}" \
            --label org.label-schema.name="${DOCKER_LABEL_NAME:-${CI_PROJECT_NAME:-}}" \
            --label org.label-schema.usage="${DOCKER_LABEL_USAGE:-${CI_PROJECT_URL:-}${CI_PROJECT_URL:+/}README.md}" \
            --label org.label-schema.vendor="${DOCKER_LABEL_VENDOR:-${CI_PROJECT_NAMESPACE:-}}" \
            --label org.label-schema.vcs-url="${DOCKER_LABEL_VCS_URL:-${CI_PROJECT_URL:-}}" \
            --label org.label-schema.vcs-ref="${DOCKER_LABEL_VCS_REF:-${CI_COMMIT_SHORT_SHA:-}}" \
            --label org.label-schema.version="${DOCKER_LABEL_VERSION:-$(get_version)}" \
            --label org.label-schema.build-date="$(date -u +"${DOCKER_LABEL_BUILD_DATE_FORMAT:-%Y-%m-%dT%H:%M:%SZ}")" \
            --label org.label-schema.docker.cmd="${DOCKER_LABEL_CMD:-docker run -it --rm ${DOCKER_IMAGE:-${CI_REGISTRY_IMAGE:-}}}" \
            --label org.label-schema.description="${DOCKER_LABEL_DESCRIPTION:-}" \
            --label org.label-schema.docker.params="${DOCKER_LABEL_PARAMS:-}" \
            --label org.label-schema.schema-version="${DOCKER_LABEL_SCHEME_VERSION:-1.0}" \
            --label org.label-schema.docker.cmd.help="${DOCKER_LABEL_CMD_HELP:-}" \
            --label org.label-schema.docker.cmd.test="${DOCKER_LABEL_CMD_TEST:-}" \
            --label org.label-schema.docker.cmd.devel="${DOCKER_LABEL_CMD_DEVEL:-}" \
            --label org.label-schema.docker.cmd.debug="${DOCKER_LABEL_CMD_DEBUG:-}" \
            --label gitlab.ci.job.id="${CI_JOB_ID:-}" \
            --label gitlab.ci.job.url="${CI_JOB_URL:-}" \
            --label gitlab.ci.runner.id="${CI_RUNNER_ID:-}" \
            --label gitlab.ci.project.id="${CI_PROJECT_ID:-}" \
            --label gitlab.ci.project.url="${CI_PROJECT_URL:-}" \
            --label gitlab.ci.pipeline.id="${CI_PIPELINE_ID:-}" \
            --label gitlab.ci.pipeline.iid="${CI_PIPELINE_IID:-}" \
            --label gitlab.ci.pipeline.url="${CI_PIPELINE_URL:-}" \
            --label gitlab.ci.concurrent.id="${CI_CONCURRENT_ID:-}" \
            --label gitlab.ci.concurrent.project.id="${CI_CONCURRENT_PROJECT_ID:-}" \
            --tag "$(get_push_image):$(get_cache_tag)" \
            --build-arg BUILDKIT_INLINE_BUILDINFO_ATTRS=1 \
            --build-arg BUILDKIT_INLINE_CACHE=1 \
            "${@:-.}"

        for tag in $(get_push_tags); do
            if [ -z "${tag}" ] || [ "${tag}" = "$(get_cache_tag)" ]; then
                continue
            fi

            docker tag "$(get_push_image):$(get_cache_tag)" "$(get_push_image):${tag}"
        done

        docker_login "$(get_push_username)" "$(get_push_registry)" "$(get_push_password)"
        docker push --all-tags "$(get_push_image)"
    }

    # It pings container port
    docker_ping_port() {
        port="$(echo "$2" | cut -d / -f 1)"

        case "$2" in
        */udp)
            nc -uzw 1 "$1" "${port}";;
        *)
            nc -zw 1 "$1" "${port}";;
        esac
    }

    # It pings container
    docker_ping() {
        name="${1:-service}"; shift || true

        info "Testing ${name} ports..."

        if [ "$#" -eq 0 ]; then
            set -- 8080/tcp 80/tcp 443/tcp
        fi

        while true; do
            for port in "$@"; do
                info "Testing ${name} port ${port}"

                if docker_ping_port "${name}" "${port}"; then
                    pass "Test passed"
                    return 0
                fi
            done

            sleep 1
        done

        fail "Test failed"
    }

    # It tests container
    docker_test() {
        docker_login "$(get_pull_username)" "$(get_pull_registry)" "$(get_pull_password)"
    }

    # It deploys image
    docker_deploy() {
        docker_pull

        for tag in ${DOCKER_PUSH_TAG:-${DOCKER_TAG:-latest}}; do
            docker_push "${tag}"
        done
    }

    # It saves CI environment variables to file
    ci_env_save() {
        if [ -z "${CI:-}" ]; then
            return
        fi

        env | \
            grep -E '^(CI_REPOSITORY_|CI_PROJECT_|CI_COMMIT_|CI_JOB_|CI_PIPELINE_|CI_ENVIRONMENT_|CI_RUNNER_|CI_SERVER_|GITLAB_USER_)' | \
            grep -vE '^(CI_SERVER_VERSION=|CI_JOB_JWT|CI_JOB_TOKEN|CI_SERVER_TLS_CA_FILE|CI_.+_PASSWORD)' | \
            cut -f 1 -d '=' | \
            while IFS= read -r name; do
                value="$(eval printf '%s' \"\$"{${name}}"\")"

                if is_empty "${value}"; then
                    continue
                fi

                # Handling JSON key
                case "${name}" in
                    CI_SERVER_VERSION_*)
                        key="$(printf '%s' "${name}" | cut -f 2- -d '_' | tr '[:upper:]' '[:lower:]' | tr '_' '.')";;
                    *)
                        key="$(printf '%s' "${name}" | cut -f 2 -d '_' | tr '[:upper:]' '[:lower:]')"
                        key="${key}.$(printf '%s' "${name}" | cut -f 3- -d '_' | to_lower_camel_case)";;
                esac

                # Handling JSON value
                case "${name}" in
                    CI_SERVER_VERSION_*|*_PORT|*_ID|*_IID)
                        ;;
                    CI_RUNNER_TAGS|CI_PROJECT_REPOSITORY_LANGUAGES)
                        value="$(printf '%s' "${value}" | tr ',' '\n' | strip | sed 's/.*/"\0"/' | tr '\n' ',' | sed 's/,$//'| \
                            sed 's/.*/[\0]/')";;
                    *_URL)
                        value="\"$(printf '%s' "${value}" | sed 's,://.*:.*@,://,g')\"";;
                    *)
                        if ! is_boolean "${value}"; then
                            value="\"${value}\""
                        fi;;
                esac

                jq --null-input ".${key} = ${value}"
            done | \
            jq --slurp 'reduce .[] as $item ({}; . *= $item)' > "${DOCKER_CI_ENV_FILE:-.ci-env.json}" || true
    }

    docker_main() {
        if [ -n "${DOCKER_EXECUTED:-}" ]; then
            return
        fi

        ci_env_save
        docker_setup

        DOCKER_FAILED=0

        case "$1" in
            lint)
                shift; docker_lint "$@";;
            test)
                shift; docker_test "$@";;
            ping)
                shift; docker_ping "$@";;
            build)
                shift; docker_build "$@";;
            deploy)
                shift; docker_deploy "$@";;
            compose)
                shift; docker-compose "$@";;
            docker|docker-compose)
                exec "$@";;
            -*)
                exec docker "$@";;
            "")
                ;;
            *)
                if docker help "$1" >/dev/null 2>&1; then
                    set -- docker "$@"
                fi

                exec "$@";;
        esac

        export DOCKER_EXECUTED=1

        return "${DOCKER_FAILED:-0}"
    }

    docker_main "$@"

.docker:
    variables:
        DOCKER_CI_IMAGE: registry.gitlab.com/tymonx/gitlab-ci/docker
        DOCKER_CI_TAG: 1.11.2
        DOCKER_DAEMON: docker
        DOCKER_VERSION: 20.10.17
        DOCKER_HOST: tcp://docker:2376
        DOCKER_TLS_CERTDIR: /certs
    image: ${DOCKER_CI_IMAGE}:${DOCKER_CI_TAG}
    services:
        - name: ${DOCKER_DAEMON}:${DOCKER_VERSION}-dind
          alias: docker
    rules:
        - if: $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_PIPELINE_SOURCE == "merge_request_event"
          when: always
          exists:
              - 'Dockerfile'
              - '**/Dockerfile'
        - when: never
    before_script:
        - set -- "${DOCKER_COMMAND:-}" ${DOCKER_ARGUMENTS:+${DOCKER_ARGUMENTS:-}}
        - *docker-script
    script:
        - set -- "${DOCKER_COMMAND:-}" ${DOCKER_ARGUMENTS:+${DOCKER_ARGUMENTS:-}}
        - *docker-script

.docker-build:
    extends: .docker
    stage: build
    variables:
        DOCKER_COMMAND: build
    artifacts:
        when: always
        paths:
            - .ci-env.json

.docker-lint:
    extends: .docker
    stage: test
    variables:
        DOCKER_COMMAND: lint
    needs: []

.docker-test:
    stage: test
    variables:
        GIT_STRATEGY: none
        DOCKER_TAG: latest
        DOCKER_IMAGE: ${CI_REGISTRY_IMAGE}
        DOCKER_TEST_IMAGE: ${DOCKER_IMAGE}:dev-${DOCKER_TAG}-${CI_PIPELINE_ID}-test
    image: ${DOCKER_TEST_IMAGE}
    rules:
        - if: $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_PIPELINE_SOURCE == "merge_request_event"
          when: always
          exists:
              - 'Dockerfile'
              - '**/Dockerfile'
        - when: never

.docker-test-image:
    extends: .docker-test

.docker-test-run:
    extends: .docker
    stage: test
    variables:
        DOCKER_TAG: latest
        DOCKER_IMAGE: ${CI_REGISTRY_IMAGE}
        DOCKER_TEST_IMAGE: ${DOCKER_IMAGE}:dev-${DOCKER_TAG}-${CI_PIPELINE_ID}-test
        DOCKER_COMMAND: test

.docker-test-compose:
    extends: .docker-test-run

.docker-test-service:
    extends: .docker
    stage: test
    variables:
        DOCKER_TAG: latest
        DOCKER_IMAGE: ${CI_REGISTRY_IMAGE}
        DOCKER_TEST_IMAGE: ${DOCKER_IMAGE}:dev-${DOCKER_TAG}-${CI_PIPELINE_ID}-test
        DOCKER_COMMAND: ping
    services:
        - name: ${DOCKER_DAEMON}:${DOCKER_VERSION}-dind
          alias: docker
        - name: ${DOCKER_TEST_IMAGE}
          alias: service

.docker-tag:
    extends: .docker
    stage: deploy
    variables:
        GIT_STRATEGY: none
        DOCKER_COMMAND: deploy
        DOCKER_PUSH_TAG: ""
    rules:
        - if: $CI_COMMIT_TAG
          when: on_success
          exists:
              - 'Dockerfile'
              - "**/Dockerfile"
        - when: never

.docker-deploy:
    extends: .docker
    stage: deploy
    variables:
        GIT_STRATEGY: none
        DOCKER_COMMAND: deploy
        DOCKER_PUSH_TAG: "-"
    rules:
        - if: $CI_COMMIT_TAG
          when: on_success
          exists:
              - 'Dockerfile'
              - "**/Dockerfile"
        - when: never

.docker-latest:
    extends: .docker
    stage: deploy
    variables:
        GIT_STRATEGY: none
        DOCKER_COMMAND: deploy
        DOCKER_PUSH_TAG: latest
    rules:
        - if: $CI_COMMIT_TAG
          when: on_success
          exists:
              - 'Dockerfile'
              - "**/Dockerfile"
        - when: never

.docker-edge:
    extends: .docker
    stage: deploy
    variables:
        GIT_STRATEGY: none
        DOCKER_COMMAND: deploy
        DOCKER_PUSH_TAG: edge
    rules:
        - if: $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
          when: on_success
          exists:
              - 'Dockerfile'
              - "**/Dockerfile"
        - when: never
...
