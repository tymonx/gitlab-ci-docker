# Copyright 2022 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG DOCKER_IMAGE=docker
ARG DOCKER_VERSION=20.10.21

ARG ALPINE_IMAGE=alpine
ARG ALPINE_VERSION=3.16.2

ARG SHELLCHECK_IMAGE=koalaman/shellcheck
ARG SHELLCHECK_VERSION=v0.8.0

ARG HADOLINT_IMAGE=hadolint/hadolint
ARG HADOLINT_VERSION=2.10.0
ARG HADOLINT_TAG=$HADOLINT_VERSION-alpine

ARG DOCKERFILE_LINTER_IMAGE=buddy/dockerfile-linter
ARG DOCKERFILE_LINTER_VERSION=1.2.2

ARG YAML_LINT_IMAGE=fleshgrinder/yamllint
ARG YAML_LINT_VERSION=1.28.0

ARG YQ_IMAGE=mikefarah/yq
ARG YQ_VERSION=4.28.2

# Run Dockerfile linter
FROM $HADOLINT_IMAGE:$HADOLINT_TAG as hadolint
COPY Dockerfile /tmp/Dockerfile
RUN hadolint --failure-threshold ignore /tmp/Dockerfile

# Run Dockerfile linter
FROM $DOCKERFILE_LINTER_IMAGE:$DOCKERFILE_LINTER_VERSION as dockerfilelinter
COPY Dockerfile /tmp/Dockerfile
RUN /dockerfilelinter/bin/dockerfilelinter --error --ignore ER0012,ER0015 --file /tmp/Dockerfile

# Run YAML linter
FROM $YAML_LINT_IMAGE:$YAML_LINT_VERSION
COPY base.yml /tmp/base.yml
COPY .yamllint.yml /usr/local/src/.yamllint.yml
RUN yamllint /tmp/base.yml

# Extract to shell script
FROM $YQ_IMAGE:$YQ_VERSION as builder
COPY base.yml /base.yml
# linter ignore=ER0001
RUN yq eval '.".docker".script[1] | explode(.)' /base.yml > /workdir/entrypoint && chmod a+x /workdir/entrypoint

# Run shell script linter
FROM $SHELLCHECK_IMAGE:$SHELLCHECK_VERSION as shellcheck
FROM $ALPINE_IMAGE:$ALPINE_VERSION
COPY --from=shellcheck /bin/shellcheck /bin/shellcheck
COPY --from=builder /workdir/entrypoint /tmp/entrypoint
RUN shellcheck --enable=all --severity=style /tmp/entrypoint

# Build Docker image
FROM $DOCKER_IMAGE:$DOCKER_VERSION

ENV DOCKER_BUILDKIT=1

# hadolint ignore=DL3008,DL3018,DL3037,DL3041
RUN \
    if command -v apt-get >/dev/null 2>&1; then \
        apt-get update && \
        apt-get install --yes --no-install-recommends --no-upgrade \
            node \
            && \
        apt-get clean && \
        apt-get autoclean && \
        rm -rf /var/lib/apt/lists/*; \
    elif command -v apk >/dev/null 2>&1; then \
        apk --no-cache update && \
        apk --no-cache add \
            nodejs \
            jq \
            && \
        rm -rf /var/cache/apk/*; \
    elif command -v dnf >/dev/null 2>&1; then \
        dnf update && \
        dnf install --assumeyes \
            nodejs \
            && \
        dnf clean all; \
    elif command -v zypper >/dev/null 2>&1; then \
        zypper update --no-confirm && \
        zypper install --no-confirm \
            nodejs \
            && \
        zypper clean --all; \
    fi

COPY --from=hadolint /bin/hadolint /usr/local/bin/hadolint
COPY --from=dockerfilelinter /dockerfilelinter /opt/dockerfilelinter
COPY --from=builder /workdir/entrypoint /usr/local/bin/entrypoint

RUN \
    docker buildx install && \
    ln -s /opt/dockerfilelinter/bin/dockerfilelinter /usr/local/bin/dockerfilelinter

ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["docker"]
